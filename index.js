

let posts = [];
let count = 1;


// Add post data
	
	//addEvenListener(event, callback function that will trigger if the event occur or happen)
	document.querySelector("#form-add-post").addEventListener("submit", (event) =>{

		/*prevent Default function stops the auto reload of the webpage when submitting*/
		event.preventDefault();

		posts.push({
			id : count,
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,

		})
		// console.log(posts);
		count++;
		showPosts(posts);
	});


//Show post
	//example of function expression -not hoisted can be involked before the function
	const showPosts = (posts) =>{
		let postEntries = '';

		posts.forEach((post) =>{
			postEntries += `<div id = "post-${post.id}">
			<h3 id = "post-title-${post.id}">${post.title}</h3>
			<p id = "post-body-${post.id}">${post.body}</p>
			<button onClick = "editPost('${post.id}')">Edit</button>
			<button onClick = "removePost('${post.id}')">Delete</button>
			</div>`
		})

		// console.log(postEntries);

		document.querySelector("#div-post-entries").innerHTML = postEntries;
	}


//Edit post
	//Fuction declaration
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	console.log(title);
	console.log(body);

	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
	document.querySelector('#txt-edit-id').value = id;
}

document.querySelector("#form-edit-post").addEventListener("submit", (event) =>{
	event.preventDefault();

	const idTobeEdited = document.querySelector('#txt-edit-id').value;

	for(let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === idTobeEdited){
			posts[i].title = document.querySelector('#txt-edit-title').value;

			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);

			alert('Edit is Sucessful');

			break;
		}
	}
})

const removePost = (id) => {
	posts.splice(id - 1, 1);
	const deletePost = document.querySelector(`#post-${id}`);
	deletePost.remove();
};
